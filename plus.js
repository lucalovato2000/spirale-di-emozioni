/*
Progetto: Estetica del codice
Autore: Luca Lovato
*/

let music;
let updateSize = 500;
let amp = new p5.Amplitude();

function preload() {
  music = loadSound("music/Serenity.mp3");
}

//crea il canvas, il colore di background, richiama smooth e fa partire la canzone
function setup() {
  createCanvas(3000, 3000);
  background(255);
  smooth();
  music.play();
}

//ferma la canzone a 60 secondi
function draw() {
  stopmusic();

  //ottiene il valore corrente della canzone
  let vol = amp.getLevel();

  //quando passa, si ha un quadrato colorato in base al frame
  if (frameCount % 10 === 0) {
    fill(frameCount * 3 % 255, frameCount * 5 % 255, frameCount * 7 % 255);

    push(); //salva l'ambiente di lavoro
    translate(1500, 1500);  //fa iniziare il cerchio in centro al canvas

    rotate(radians(frameCount * 2 % 360)); //Fa rotare le linee del cerchio in base al framecount

    //una volta che i frame raggiungono il valore 280, crea quadrati sullo schermo
    if (frameCount >= 280) {
      updateRectangles(vol);
    }

    //per tutti i multipli di 180 frame rimuove tutti gli oggetti visibili sullo schermo
    if (frameCount % 180 === 0) {
      changeBackground();
    }
    pop();
  }

  /*incrementa costantemente il valore della variabile updateSize, tranne quando supera 300
  decrementa il suo valore a quello di dafult(300)  */
  updateSize = updateSize++ < 800 ? updateSize : 300;
  updateCircle(vol, updateSize);
}

function updateRectangles(vol) {
  strokeWeight(3); //incrementa la grandezza della linea del nuovo quadrato
  
  //ottieni il volume della canzone e con quello cambia la grandezza del quadrato
  rect(0, 0, 2000 * vol, 2000 * vol);
}

//cambia la grandezza del cerchio e lo spessore della linea
//la grandezza dipende dal vol e del valore della variabile updateSize
function updateCircle(vol, updateSize) {
  strokeWeight(3);
  ellipse(1500, 1500, vol * updateSize, vol * updateSize);
}

function changeBackground() {
  let colors = random(0, 255); //colori generati a random
  background(255, random(255), random(255)); //colore del background generato a random
}

//se il tempo della canzone supera 10 secondi si ferma
function stopmusic() {
  if (music.currentTime() > 60) {
    noLoop();
    music.stop();
  }
}